#Imports
from __future__ import print_function
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi #set tau value

    def dist(p, q): #define the distance equation
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))

class DrawInitals(object):
    #Initalization
    def __init__(self): 
        
        #Initialize rospy commander and rospy node
        moveit_commander.roscpp_initialize(sys.argv) 
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True) #Rospy node
        
        #Instantiate a RobotCommander object
        robot = moveit_commander.RobotCommander() 
        
        #Instatiate a planningsceneinterface. Provides remote interface
        scene = moveit_commander.PlanningSceneInterface()
        
        group_name = "manipulator" #Name of the robot arm planning group. Manipulator interface with Gazebo UR5
        
        #Instantiate a movegroupcommander. Interface to a planning group (joints)
        move_group = moveit_commander.MoveGroupCommander(group_name)

        #Display the robots trajectories in Rviz
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        self.move_group = move_group

    def go_to_joint_state(self):
        #PLANNING TO A JOINT GOAL, SETTING INITIAL POSITION

        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values() #Do not begin at 0, this is joint limits
        
        #Starting point: found using RVIZ Moveit
        joint_goal[0] = 0.16717167476186656
        joint_goal[1] = -1.5703653163217828
        joint_goal[2] = 1.7045257515222856
        joint_goal[3] = -0.13415460335440788
        joint_goal[4] = 0.166588592934394
        joint_goal[5] = 0.013760017695892384

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

    def plan_cartesian_path(self, scale=1.0):  
        #CARTESIAN PATH, HERE WE WILL SET HOW END EFFECTOR MOVES
 
        move_group = self.move_group       
        waypoints = [] #Each movement must be appended to this

        wpose = move_group.get_current_pose().pose

        #Draw the Y
        wpose.position.z += scale * 0.15 # Move up
        waypoints.append(copy.deepcopy(wpose)) #Add movement to the end of waypoints

        wpose.position.z += scale * 0.1 # Diagonal: left and up
        wpose.position.y -= scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1 # Diagonal: right and down
        wpose.position.y += scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.1 # Diagonal: right and up
        wpose.position.y += scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1 # Diagonal: left and down
        wpose.position.y -= scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        #Draw the L
        wpose.position.z -= scale * 0.15  # Move down
        waypoints.append(copy.deepcopy(wpose)) 

        wpose.position.y += scale * 0.1  # Move right
        waypoints.append(copy.deepcopy(wpose)) 

        # Draw the R
        wpose.position.z += scale * 0.1 # Diagonal: left and up
        wpose.position.y -= scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.1 # Move up
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * 0.1 # Move right
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1 # Move down
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y -= scale * 0.1 # Move left
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1 # Move down
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def execute_plan(self, plan):
        move_group = self.move_group
        move_group.execute(plan, wait=True) #Execute the movement in Gazebo

def main():
    try:
        tutorial = DrawInitals() #Call DrawInitals

        tutorial.go_to_joint_state()

        input("============ Press `Enter` to plan and display a Cartesian path ...") 
        #Wait for 'enter' before running Cartersian path
        cartesian_plan, fraction = tutorial.plan_cartesian_path()
        #Execute the cartersian plan
        tutorial.execute_plan(cartesian_plan)

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()