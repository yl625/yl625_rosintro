# Python 2/3 compatibility imports
from __future__ import print_function
from shutil import move
#from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math as m
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv) #initialize the commander
rospy.init_node("move_group_python_interface_tutorial", anonymous=True) #add a rospy node

robot = moveit_commander.RobotCommander() #instantiating RobotCommander object
scene = moveit_commander.PlanningSceneInterface() #instantiate PlanningSceneInterface

group_name = "manipulator" #groupname 'manipulator' to interface with gazebo UR5
move_group = moveit_commander.MoveGroupCommander(group_name)

#display trajectory in RVIZ
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

joint_goal = move_group.get_current_joint_values()
print(joint_goal) #PRINT CURRENT JOINT POSITIONS

#Go command can be called wiht joint values, poses, or without any
#parameters if you ave already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

#Calling ''stop()'' ensures that there is no residual movement
move_group.stop()