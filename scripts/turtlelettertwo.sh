#!/usr/bin/bash

rosservice call /turtle1/set_pen 80 190 20 3 off
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-1.0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0, -1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 6.5 5.5 0.0 "turtle2"
rosservice call /turtle2/set_pen 100 30 200 3 off
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.5, 3.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.5, -3.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-0.75, 1.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
