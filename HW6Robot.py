# ECE383 HW6, Inital L
from __future__ import print_function
#from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi #set tau value

    def dist(p, q): #define the distance equation
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


#BEGIN THE CODE
moveit_commander.roscpp_initialize(sys.argv) #Initialize rospy commander
rospy.init_node("move_group_python_interface_tutorial", anonymous=True) #Rospy node

robot = moveit_commander.RobotCommander() #Instantiate a RobotCommander object

scene = moveit_commander.PlanningSceneInterface() #Instatiate a PlanningSceneInterface object: remote interface

#Instantiate a MoveGroupCommander: interface to the joints
group_name = "manipulator" #Name of the robot arm planning group. Manipulator interface with Gazebo UR5
move_group = moveit_commander.MoveGroupCommander(group_name)

#Display the robots trajectories in Rviz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)


#PLANNING TO A JOINT GOAL, SETTING INITIAL POSITION
# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values() #Do not begin at 0, this is joint limits
#Starting point: found using RVIZ Moveit
joint_goal[0] = 0.16717167476186656
joint_goal[1] = -1.5703653163217828
joint_goal[2] = 1.7045257515222856
joint_goal[3] = -0.13415460335440788
joint_goal[4] = 0.166588592934394
joint_goal[5] = 0.013760017695892384

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()


#CARTESIAN PATH, HERE WE WILL SET HOW END EFFECTOR MOVES
scale = 1.0
waypoints = [] #Each movement must be appended to this

wpose = move_group.get_current_pose().pose

#First movement: sketch bottom of L - move left 
wpose.position.y -= scale * 0.15  # First move sideways to the left(y)
waypoints.append(copy.deepcopy(wpose)) #Add movement to the end of waypoints

wpose.position.z += scale * 0.2  # Second move upwards (z)
waypoints.append(copy.deepcopy(wpose)) #Add movement to the end of waypoints

(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

# Note: We are just planning, not asking move_group to actually move the robot yet:
#return plan, fraction

move_group.execute(plan, wait=True) #Execute the movement in Gazebo