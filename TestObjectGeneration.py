#Imports
from __future__ import print_function
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Point, Pose, Quaternion

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi #set tau value

    def dist(p, q): #define the distance equation
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class DrawInitals(object):
    #Initalization
    def __init__(self): 

        #Initialize rospy commander and rospy node
        moveit_commander.roscpp_initialize(sys.argv) 
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True) #Rospy node
        
        #Instantiate a RobotCommander object
        robot = moveit_commander.RobotCommander() 
        
        #Instatiate a planningsceneinterface. Provides remote interface
        scene = moveit_commander.PlanningSceneInterface()
        
        group_name = "manipulator" #Name of the robot arm planning group. Manipulator interface with Gazebo UR5
        
        #Instantiate a movegroupcommander. Interface to a planning group (joints)
        move_group = moveit_commander.MoveGroupCommander(group_name)

        #Display the robots trajectories in Rviz
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the robot
        #print("============ Printing robot state")
        #print(robot.get_current_state())
        #print("")

        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names
    
    def go_to_joint_state(self):
        #PLANNING TO A JOINT GOAL, SETTING INITIAL POSITION

        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values() #Do not begin at 0, this is joint limits
        
        #Starting point: found using RVIZ Moveit
        joint_goal[0] = 0.16717167476186656
        joint_goal[1] = -1.5703653163217828
        joint_goal[2] = 1.7045257515222856
        joint_goal[3] = -0.13415460335440788
        joint_goal[4] = 0.166588592934394
        joint_goal[5] = 0.013760017695892384

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

    def plan_cartesian_path(self, scale=1.0):  
        #CARTESIAN PATH, HERE WE WILL SET HOW END EFFECTOR MOVES
 
        move_group = self.move_group       
        waypoints = [] #Each movement must be appended to this

        wpose = move_group.get_current_pose().pose

        #Draw the Y
        wpose.position.z += scale * 0.15 # Move up
        waypoints.append(copy.deepcopy(wpose)) #Add movement to the end of waypoints

        wpose.position.z += scale * 0.1 # Diagonal: left and up
        wpose.position.y -= scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1 # Diagonal: right and down
        wpose.position.y += scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.1 # Diagonal: right and up
        wpose.position.y += scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.1 # Diagonal: left and down
        wpose.position.y -= scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        #Draw the L
        wpose.position.z -= scale * 0.15  # Move down
        waypoints.append(copy.deepcopy(wpose)) 

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def execute_plan(self, plan):
        move_group = self.move_group
        move_group.execute(plan, wait=True) #Execute the movement in Gazebo

    def wait_for_state_update(self, box_is_known=False, box_is_attached=False, timeout=4):
        box_name = self.box_name
        scene = self.scene

        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = scene.get_attached_objects([box_name])
            is_attached = len(attached_objects.keys()) > 0

            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            is_known = box_name in scene.get_known_object_names()

            # Test if we are in the expected state
            if (box_is_attached == is_attached) and (box_is_known == is_known):
                return True

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()

        # If we exited the while loop without returning then we timed out
        return False

    def spawn_object(file_name, number, x, y, z=0.0254):
        client = rospy.ServiceProxy("/gazebo/spawn_sdf_model", SpawnModel)
        path = "/home/lmt53/catkin_ws/src/final_dependencies/src/chess_files_cws/" + file_name + ".sdf"
        client(
            model_name = file_name + str(number),
            model_xml = open(path, "r").read(),
            robot_namespace = "/moveit_commander",
            initial_pose = Pose(position = Point(x, y, z),
            orientation = Quaternion(0, 0, 0, 0)),
            reference_frame = "world"
        )


def main():
    try:
        tutorial = DrawInitals() #Call DrawInitals

        tutorial.go_to_joint_state()

        input("============ Press `Enter` to plan and display a Cartesian path ...") 
        #Wait for 'enter' before running Cartersian path
        cartesian_plan, fraction = tutorial.plan_cartesian_path()
        #Execute the cartersian plan
        tutorial.execute_plan(cartesian_plan)

        tutorial.spawn_object('board', 0, 0.508, 0.508, 0)
        tutorial.spawn_object('white_pawn', 1, 0.28575, 0.34925, 0.0254)
        tutorial.spawn_object('white_pawn', 2, 0.34925, 0.34925, 0.0254)

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()
